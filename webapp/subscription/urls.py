from django.urls import path
# import views for subscription
from . import views

# define application name space
app_name = 'subscription'

urlpatterns = [
    path('', views.landing, name='landing'),
    path('account', views.account, name='acount'),
    path('subscribe/<str:id>', views.subscribe, name='subscribe'),
    path('unsubscribe/<str:id>', views.unsubscribe, name='unsubscribe')
]
