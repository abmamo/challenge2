from django.apps import AppConfig

# configure subscription application


class SubscriptionConfig(AppConfig):
    name = 'subscription'
