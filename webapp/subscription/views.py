from datetime import datetime
# for template rendering
from django.shortcuts import render, redirect
# to get stripe settings from app settings
from django.conf import settings
# to secure routes with login
from django.contrib.auth.decorators import login_required
# to allow only specific http methods on certain routes (like post only for e.g)
from django.views.decorators.http import require_http_methods
# to show alerts to user
from django.contrib import messages
# import subscription model
from subscription.models import Subscription
# import stripe
import stripe
from datetime import datetime, date

# set stripe api-key from settings
stripe.api_key = settings.STRIPE_SECRET_KEY


def landing(request):
    '''landing page where user either signs in or signs out'''
    return render(request, 'landing.html')

# require login
@login_required(login_url='/login')
def account(request):
    # get subscription in database
    subscription = Subscription.objects.filter(
        user=request.user).first()
    # create context variable to pass to template
    context = {'status': subscription.status,
               'stripe_id': subscription.stripe_id}
    # render template
    return render(request, 'account.html', context=context)

# require login
@login_required(login_url='/login')
def subscribe(request, id):
    # get subscription in database
    subscription = Subscription.objects.filter(stripe_id=id).first()
    # get subscription from stripe
    response = stripe.Subscription.retrieve(
        subscription.stripe_id)
    # update stripe
    response = stripe.Subscription.modify(
        response.id,
        cancel_at_period_end=False,
        proration_behavior='create_prorations',
        items=[{
            'id': response['items']['data'][0].id,
            'plan': response['items']['data'][0].plan['id']
        }]
    )
    # this is update step for local db
    subscription.status = response['status']
    # save changes in db
    subscription.save()
    # alert user
    messages.error(request, "Subscribed to premium.")
    # render cms
    return redirect('/subscription/account')


@login_required(login_url='/login')
def unsubscribe(request, id):
    # get subscription in database using stripe_id
    subscription = Subscription.objects.filter(stripe_id=id).first()
    # check status and modify stripe subscription accordingly
    if subscription.status == 'trialing':
        # get subscription from stripe and update
        response = stripe.Subscription.modify(
            subscription.stripe_id, cancel_at_period_end=True, trial_end='now')
        # this is update step for local db
        subscription.status = response['status']
        # save changes in db
        subscription.save()
        # alert user
        messages.error(request, "Unsubscribed from trial.")
        # render cms
        return redirect('/subscription/account')
    elif subscription.status == 'active':
        # get subscription from stripe and update
        response = stripe.Subscription.modify(
            subscription.stripe_id, cancel_at_period_end=True)
        # this is update step for local db
        subscription.status = 'canceled'
        # save changes in db
        subscription.save()
        # alert user
        messages.error(request, "Unsubscribed from premium.")
        # render cms
        return redirect('/subscription/account')
