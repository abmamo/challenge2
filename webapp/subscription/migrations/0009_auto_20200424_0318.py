# Generated by Django 3.0.5 on 2020-04-24 03:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0008_auto_20200424_0314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='created_on',
            field=models.DateField(auto_now_add=True),
        ),
    ]
