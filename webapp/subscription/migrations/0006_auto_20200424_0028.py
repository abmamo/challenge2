# Generated by Django 3.0.5 on 2020-04-24 00:28

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0005_auto_20200424_0010'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscription',
            name='name',
        ),
        migrations.AlterField(
            model_name='subscription',
            name='created_on',
            field=models.DateField(verbose_name=datetime.datetime(2020, 4, 24, 0, 28, 29, 976, tzinfo=utc)),
        ),
    ]
