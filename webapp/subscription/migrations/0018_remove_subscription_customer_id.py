# Generated by Django 3.0.5 on 2020-04-30 05:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0017_subscription_customer_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscription',
            name='customer_id',
        ),
    ]
