# Generated by Django 3.0.5 on 2020-04-24 03:14

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('subscription', '0007_auto_20200424_0030'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subscription',
            name='created_on',
            field=models.DateField(default=datetime.datetime(2020, 4, 24, 3, 14, 39, 90370, tzinfo=utc)),
        ),
    ]
