from django.test import TestCase

from .models import Subscription


class SubscriptionModelTest(TestCase):

    def test_string_representation(self):
        subscription = Subscription(
            stripe_id="subscription_stripe_id", name="subscription_name")
        self.assertEqual(str(subscription), subscription.name)
