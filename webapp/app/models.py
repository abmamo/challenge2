# import user model for event detection
from django.contrib.auth.models import User
# import post save to find out post save
from django.db.models.signals import post_save
from django.conf import settings
# import receiver to recieve signal
from django.dispatch import receiver
# import subscription model to save to local database
from subscription.models import Subscription
# import stripe python library
import stripe
# set api key to make requests
stripe.api_key = settings.STRIPE_SECRET_KEY


@receiver(post_save, sender=User)
def create_subscription(sender, **kwargs):
    '''
        django signal used to create subscription for a customer
        once it is created using product configuration info
        saved in settings.
    '''
    if kwargs.get('created', True):
        # get user
        user = kwargs.get('instance')
        # get user email
        email = user.email
        # create fake payment method for user / card that passes in stripe
        payment_method = stripe.PaymentMethod.create(
            type="card",
            card={
                "number": "4242424242424242",
                "exp_month": 4,
                "exp_year": 2021,
                "cvc": "314",
            },
        )
        # create stripe customer using email & a default payment
        # if this was in production would use checkout.js to
        # get payment information and attach to customer
        customer = stripe.Customer.create(
            email=email,
            payment_method=payment_method.id,
            invoice_settings={
                'default_payment_method': payment_method.id,
            }
        )

        # create & save subscription for customer in stripe and get response object
        # containing info about subscription
        response = stripe.Subscription.create(
            customer=customer.id,
            plan=settings.PLAN_ID,
            trial_period_days=settings.TRIAL_PERIOD_DAYS,
            expand=['latest_invoice.payment_intent']
        )
        # create subscription on disk
        subscription = Subscription(
            # get the stripe id from the stripe response
            stripe_id=response.stripe_id, user=user, status=response['status'])
        # save subscription to database
        subscription.save()
