
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm
from .forms import SignupForm
from django.conf import settings
from django.shortcuts import render, redirect
# import messages to show messages
from django.contrib import messages
# import models
from app import models

# Create your views here.


def signin(request):
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        # check if form submission is valid
        if form.is_valid():
            # get username and password
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            # check if credentials match any user
            user = authenticate(username=username, password=password)
            if user is not None:
                # user exists login user and redirect to account page
                login(request, user)
                return redirect(request.GET.get('next', 'subscription/account'))
            else:
                # alert user credentials don't exist
                messages.error(request, "Invalid username or password.")
        else:
            messages.error(request, "Invalid username or password.")
    form = AuthenticationForm()
    return render(request=request,
                  template_name="login.html",
                  context={"form": form})


def signout(request):
    # logout user that sent the request
    logout(request)
    # show alert & redirect
    messages.info(request, "Logged out successfully!")
    return redirect("/")


def error_404(request, exception):
    return render(request, '404.html', status=404)


def error_500(request):
    return render(request, '500.html', status=500)
