"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.views.generic import RedirectView
from django.conf import settings
from django.conf.urls import url, handler404, handler500
from django.conf.urls.static import static
from . import views
import stripe
stripe.api_key = settings.STRIPE_SECRET_KEY

handler404 = 'app.views.error_404'
handler500 = 'app.views.error_500'


urlpatterns = [

    path('subscription/', include('subscription.urls')),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path("login", views.signin, name="login"),
    path("logout", views.signout, name="logout"),
    path('admin/', admin.site.urls),
]

# since the only application is going to be the subscription application
# redirect the root of the site to the subscriptions app
urlpatterns += [
    path('', RedirectView.as_view(url='subscription/', permanent=True)),
]

# Use static() to add url mapping to serve static files during development (only
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# one time script to create product in stripe / thought middleware here would be overkill
# get plan from stripe
try:
    stripe.Plan.retrieve(settings.PLAN_ID)
except:
    # create plan using configuration setting in app
    plan = stripe.Plan.create(
        amount=settings.AMOUNT,
        currency=settings.CURRENCY,
        interval=settings.INTERVAL,
        trial_period_days=settings.TRIAL_PERIOD_DAYS,
        product={"name": settings.PRODUCT_NAME},
    )
    # set PLAN_ID in settings
    settings.PLAN_ID = plan.id
