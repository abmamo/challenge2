# README #

Django web application built using Django + Django Registration + PostgreSQL/SQLite + Bootstrap 4 + Stripe to manage a WhatsBusy Premium 
subscription.

### How do I get set up? ###

In order to run this application you need to have python, pip and virtualenv installed. Documentation can be found [here](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) 
Since the application requires stripe integrations I have created a dummy stripe account and saved the credentials in settings.py (this would definitely need to change to using environment variables if using in a production context)
Therefore the application should work if all the dependencies in requirements.txt are installed.

#### Installation ###

* clone responsitory and navigate to directory
```
	git clone https://abmamo@bitbucket.org/abmamo/challenge2.git && cd challenge2
```
* create and activate virtual environment
```
	python3 -m venv env && source env/bin/activate
```
* Dependencies
```
	pip install -r requirements.txt
```
* Start the web app using
```
	python3 webapp/manage.py runserver
```

#### Getting Started ###

* sign up for an account
```
	localhost:8000/accounts/register (the default Django registration URL path)
```
* log into account
```
	localhost:8000/login (custom login page)
```
* account page must
```
	 show account page with status “trialing” and an action button “unsubscribe”
```
* if unsubscribed
```
	status will change to “active” (because you cancel trial)
```
* if unsubscribed
```
	status will change to “canceled” and action button to “subscribe”
```

### Limitations ###
- Instead of subscribing user immediately on signup as per the instructions, add a page on registration completion that requires the user to add a payment method (currently the application works by setting a default payment method at signup)
- Add conditions to handle other stripe life cycle events (canceled, active, trialing, incomplete_expired, past_due, Unpaid) The application currently has only 3 of these implemented)

### Notes ###

Initially implemented user signup with my own methods which allowed me to create a stripe subscription on user creation. Since Django-registration does not have a way to customize the signup process decided to use Django signals to create the subscription for the user after signup. Signals in Django follow the observer design pattern. Have two elements: sender and receiver. A receiver must be a function or an instance method which is to receive signals. A sender must either be a Python object, or None to receive events from any sender. The connection between the senders and the receivers is done through “signal dispatchers”, which are instances of Signal, via the connect method. We need to make the signal dispatcher accessible to the app early on so the dispatcher lives in models.py (need to import models.py for the signal to be executed). The signal gets the post_save signal from the User class and creates a subscription in stripe and in the local database. At this point I realized the subscription object is actually stored in stripe and the local database just serves as a partial information container / gateway. Therefore we only need to maintain the stripe subscription id and the user in local database.